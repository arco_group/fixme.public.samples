// -*- coding:utf-8; mode:c++ -*-

#include <iostream>

#include <dummy.h>

using namespace std;

int main(int argc, char** argv) {
  dummy d;
  cout << "Hola Mundo " << d.f(4) << endl;
  return 0;
}
